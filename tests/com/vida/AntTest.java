package com.vida;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

class AntTest {

    @Test
    void ant() {
        Ant randy = new Ant("Allo!");
        String result = randy.getMessage();
        Assertions.assertEquals("Allo!",result);
    }

    @Test
    void getMessage() {
        Ant randy = new Ant();
        String result = randy.getMessage();
        Assertions.assertEquals("Ola mundo, eu estou vivo!",result);
    }


    @Test
    void setMessage() {
        Ant randy = new Ant();
        randy.setMessage("Hello World");
        Assertions.assertEquals("Hello World",randy.getMessage());
    }

    @Test
    void getMessageWithNull() {
        Ant randy = new Ant();
        //Check to make sure a null is sent
        Assertions.assertThrows(NullPointerException.class,
                ()->{
                    randy.setMessage(null);
                });
    }
}