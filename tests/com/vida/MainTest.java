package com.vida;

import org.junit.jupiter.api.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import static org.junit.jupiter.api.Assertions.assertEquals;


class MainTest {
    //capture the stream
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @AfterEach
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    @Test
    void mainWithArgsNull() {
        Main.main(null);
        String result = "Ola mundo, eu estou vivo!" + System.lineSeparator();
        assertEquals(result, outContent.toString());
    }

    @Test
    void mainWithArgsEmpty() {
        String[] args = new String[]{};
        Main.main(args);
        String result = "Ola mundo, eu estou vivo!" + System.lineSeparator();
        assertEquals(result, outContent.toString());
    }
    @Test
    void mainWithArgs() {
        String[] args = new String[]{"x"};
        Main.main(args);
        String result = "Ola mundo, eu estou vivo!" + System.lineSeparator();
        assertEquals(result, outContent.toString());
    }
}