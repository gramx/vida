package com.vida;

import java.io.PrintStream;

public class Main {

    public static void sayHello(PrintStream out){
        out.println(new Ant().getMessage());
    }

    public static void main(String[] args) {
        sayHello(System.out);
    }
}
